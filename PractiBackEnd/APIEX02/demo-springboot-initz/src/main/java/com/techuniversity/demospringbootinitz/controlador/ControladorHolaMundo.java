package com.techuniversity.demospringbootinitz.controlador;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.lang.module.ResolutionException;

@RestController
@RequestMapping("${api.saludos.v1}")
public class ControladorHolaMundo {

    String saludo = "Holi";

    //@RequestMapping(method = RequestMethod.GET, path = "/api-hola/v1/saludos")
    @GetMapping //("/api-hola/v1/saludos")
    public String saludar(
            @RequestParam(name = "saludar", required=false) String nombre
            //@RequestParam(required = false) String saludo
    ){

       if (nombre == null || nombre.isEmpty())
           nombre = "Mundo";
       if (saludo == null || saludo.isEmpty())
            saludo = "Salut";

       if(nombre.equalsIgnoreCase("Voldemort")) {
           throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

       }

       return String.format("¡%s, %s!", saludo, nombre);
        //"¡Hola Mundo!";

    }

    @PostMapping //("/api-hola/v1/saludos")
    public void agregarSaludos(@RequestBody String saludo){
        this.saludo = saludo;

    }

    @DeleteMapping //("/api-hola/v1/saludos")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarSaludo()
    {
        this.saludo = "Aló";
    }

}
