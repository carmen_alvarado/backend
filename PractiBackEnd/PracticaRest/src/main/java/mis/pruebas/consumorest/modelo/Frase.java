package mis.pruebas.consumorest.modelo;

public class Frase {
    String type;
    ValorFrase value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ValorFrase getValue() {
        return value;
    }

    public void setValue(ValorFrase value) {
        this.value = value;
    }
}
