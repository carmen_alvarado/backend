package mis.pruebas.consumorest.controlador;

import mis.pruebas.consumorest.modelo.Frase;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api-frases/v1/frases")
public class ControladorFrases {

    static final String urlFrases = "https://quoters.apps.pcfone.io/api/random";

    @GetMapping
    public String obtenerFrase(){
        final RestTemplate template = new RestTemplate();

        ResponseEntity<Frase> respuesta = template.getForEntity(urlFrases, Frase.class);
        //if (respuesta.getStatusCode() == HttpStatus.OK)
        //Acá como lo vimos en CORS if (respuesta.getHeaders().getAllow())
        if (respuesta.getStatusCode().isError())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

            //Para mandar un objeto
        //template.postForObject()
        final Frase frase = respuesta.getBody();
        //final Frase frase = template.getForObject(urlFrases,Frase.class);

        return frase.getValue().getQuote();

    }

}
