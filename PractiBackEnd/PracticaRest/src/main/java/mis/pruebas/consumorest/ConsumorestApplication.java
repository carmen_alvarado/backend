package mis.pruebas.consumorest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumorestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumorestApplication.class, args);
	}

}
