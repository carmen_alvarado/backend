package mis.pruebas.carritoproductos.controlador;

import com.sun.management.GarbageCollectionNotificationInfo;
import mis.pruebas.carritoproductos.modelo.Producto;
import mis.pruebas.carritoproductos.modelo.Proveedor;
import mis.pruebas.carritoproductos.servicio.ServicioGenerico;
import mis.pruebas.carritoproductos.servicio.impl.ServicioGenericoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
//@RequestMapping("${api.productos.v1}/{idProducto}/proveedores")
@RequestMapping("/almacen/v1/productos/{idProducto}/proveedores")
public class ControladorProveedoresProducto {

    @Autowired
    ServicioGenerico<Producto> servicioProducto;
    //ServicioGenerico<Producto> servicioProducto = new ServicioGenericoImpl<>();

    @Autowired
    ServicioGenerico<Proveedor> servicioProveedor;
    //ServicioGenerico<Proveedor> servicioProveedor = new ServicioGenericoImpl<>();

    @GetMapping
    public CollectionModel<EntityModel<Proveedor>> obtenerProveedoresProducto(@PathVariable long idProducto) {

        //"self" "http://localhost:9999/almacen/v1/productos/1/proveedores"
        //"producto" "http://localhost:9999/almacen/v1/productos/1"

        final List<Integer> idProvs = this.servicioProducto.obtenerPorId(idProducto).getIdsProveedores();
        final List<Proveedor> provsXId = idProvs.stream().map(idProveedor -> this.servicioProveedor.obtenerPorId(idProveedor)).collect(Collectors.toList());
        //CollectionModel<EntityModel<Proveedor>> col =
        return CollectionModel.of(provsXId.stream().map(p -> EntityModel.of(p)
        .add(linkTo(methodOn(ControladorProveedor.class).obtenerProveedorPorId(p.getId())).withSelfRel())

        ).collect(Collectors.toList()))
                .add(linkTo(methodOn(ControladorProducto.class).obtenerProductoPorId(idProducto)).withRel("producto"))
                .add(linkTo(methodOn(this.getClass()).obtenerProveedoresProducto(idProducto)).withSelfRel());
        //return col;
    }

    //public List<Integer> obtenerProveedoresProducto(@PathVariable long idProducto) {
    //   final List<Integer> idProvs = this.servicioProducto.obtenerPorId(idProducto).getIdsProveedores();
    //  // final List<Proveedor> provXId = idProvs.stream().map(idProveedor -> this.servicioProveedor.obtenerPorId(idProveedor)).collect(Collectors.toList());
    //   //ids.stream().map(id -> linkTo(methodOn(ControladorProveedor.class).obtenerProveedorPorId(id)));
    //   // return this.servicioProducto.obtenerPorId(idProducto).getIdsProveedores();
    //    return idProvs;
    //}



    @GetMapping("/{indiceProveedor}")
    public Proveedor obtenerProveedoresProducto(@PathVariable long idProducto,
                                                @PathVariable int indiceProveedor) {
        final long idProveedor = this.servicioProducto
                .obtenerPorId(idProducto)
                .getIdsProveedores().get(indiceProveedor);
        return this.servicioProveedor.obtenerPorId(idProveedor);
    }

    static class ProveedorProducto {
        public int idProveedor;
    }

    @PostMapping
    public void agregarProveedorProducto(@PathVariable long idProducto,
                                         @RequestBody ProveedorProducto prov) {
        try {
            this.servicioProveedor.obtenerPorId(prov.idProveedor);
            this.servicioProducto.obtenerPorId(idProducto)
                    .getIdsProveedores().add(prov.idProveedor);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{indiceProveedorProducto}")
    public void borrarProveedorProducto(@PathVariable long idProducto,
                                        @PathVariable int indiceProveedorProducto) {
        try {
            final List<Integer> listaIds = this.servicioProducto
                    .obtenerPorId(idProducto)
                    .getIdsProveedores();
            try {
                listaIds.remove(indiceProveedorProducto);
            } catch(Exception x) {}
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
