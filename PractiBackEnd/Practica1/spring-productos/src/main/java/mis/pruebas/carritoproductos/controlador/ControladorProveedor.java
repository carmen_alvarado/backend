package mis.pruebas.carritoproductos.controlador;

import mis.pruebas.carritoproductos.modelo.Producto;
import mis.pruebas.carritoproductos.modelo.Proveedor;


//import org.springframework.beans.factory.annotation.Autowired;
import mis.pruebas.carritoproductos.servicio.ServicioGenerico;
import mis.pruebas.carritoproductos.servicio.impl.ServicioGenericoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
//@RequestMapping("${api.proveedores.v1}")
@RequestMapping("/almacen/v1/proveedores")
@ExposesResourceFor(Proveedor.class)
public class ControladorProveedor {

    @Autowired
    ServicioGenerico<Proveedor> servicioProveedor; // Inyección de Dependencias
    //ServicioGenerico<Proveedor> servicioProveedor = new ServicioGenericoImpl<>();

    @Autowired
    EntityLinks entityLinks;


    @GetMapping
    public CollectionModel<EntityModel<Proveedor>> obtenerProveedores() {
        //return Arrays.asList(new Proveedor(1L,"Zapatillas Locas",5000.00, 300));
        //return this.servicioProveedor.obtenerProveedores();
        final Map<Long, Proveedor> mp = this.servicioProveedor.obtenerTodos();
        final List<Proveedor> pp = new ArrayList<>();
        for(Long id: mp.keySet()) {
            final Proveedor p = mp.get(id);
            p.setId(id);
            pp.add(p);
        }return CollectionModel.of(
                pp.stream().map(p -> EntityModel.of(p).add(crearEnlaceProveedor(p))).collect(Collectors.toUnmodifiableList()));
    }

    @GetMapping("/{idProveedor}")
    //EntityModel<Proveedor>
    public Proveedor obtenerProveedorPorId(@PathVariable(name = "idProveedor") long id) {
        try {
            final Proveedor p = this.servicioProveedor.obtenerPorId(id);
            return p;
            //return EntityModel.of(p).add(crearEnlaceProveedor(p));

        } catch (Exception x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


    //@PostMapping
    //public void crearProveedor(@RequestBody Proveedor p) {
    //    final long id = this.servicioProveedor.agregar(p);
    //    p.setId(id);
    //}

    @PostMapping
    public ResponseEntity<EntityModel<Proveedor>> crearProveedor(@RequestBody Proveedor p) {
        // Location: http://localhost:9999/almacen/v1/productos/1
        final long id = this.servicioProveedor.agregar(p);
        p.setId(id);
        return ResponseEntity
                .ok()
                .location(crearEnlaceProveedor(p).toUri())
                .body(EntityModel.of(p).add(crearEnlaceProveedor(p)))
                ;
    }





    @PutMapping("/{idProveedor}")
    public void reemplazarProveedorPorId(@PathVariable(name = "idProveedor") long id,
                                         @RequestBody Proveedor p) {
        try {
            this.servicioProveedor.reemplazarPorId(id, p);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{idProveedor}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProveedorPorId(@PathVariable(name = "idProveedor") long id) {
        this.servicioProveedor.borrarPorId(id);
    }

    //Métodos auxiliares
    private Link crearEnlaceProveedor(Proveedor p) {
        return this.entityLinks
                .linkToItemResource(p.getClass(), p.getId())
                .withSelfRel()
                .withTitle("Detalles de este proveedor");
    }





}




