package mis.pruebas.consumoproductos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumoRestProductosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumoRestProductosApplication.class, args);
	}

}
